![VenPlugPlus logo](https://codeberg.org/Mopigames/VenPlugPlus/raw/branch/main/assets/venplugplus-128x128.png)


# VenPlugPlus

Discord go brrrrrrrrrrrrrrrrrrrrrr (fork of https://github.com/KaydaFox/vencord-buttplugio)

## Requirements

- A copy of the Intiface central software available [here!](https://intiface.com/central/)
- Discord (duh)
- An [intiface compatible hardware](https://iostindex.com/)
- A bit of patience

## How to install

- First follow the instructions [here](https://docs.vencord.dev/installing/) and stop right before the `pnpm build` command. 
- Run `pnpm install buttplug -w`
- Create a folder inside `src` called `userplugins` and drop [this file](https://codeberg.org/Mopigames/VenPlugPlus/raw/branch/main/index.ts) in it. 
- continue following the vencord installation instructions until you launch the discord client
- go to your settings, enable the plugin and you're good to go! :3
- Have fun~ hehe

## How to use

### wip (feel free to make a pr im lazy and tired)

- First open the plugin settings. You will see the available options:

### Connect automatically
If true, it will connect to intiface on startup. (With this off, you need to re-enable the plugin to reconnect)

### Ramp up and down
If true, it will try and smoothly ramp the vibration intensity up and down

### Steps slider
How many steps to use when ramping up and down

### Websocket URL
The URL of the websocket server

### Vibration intensity slider
The maximum vibration intensity

### Target words
Words to use as targets when you weren't mentionned. Example: [your nickname]

### Trigger words
Words to use as triggers. when someone uses these words, it will vibrate. Eg: "good girl, etc.."

### Add-on words
Words that will be used as a bonus vibration. The more add-on words, the bigger the vibration. Eg: "Very, such a, etc..."

### Blacklist/witelist stuff
Settings related to blacklisting or whitelisting users, channels and guilds.

### Rich presence
Enable Discord Rich Presence. This will display information such as currently connected devices, vibration intensity, intiface status and more!

### Rich presence name
Allows you to set a custom name for the rich presence!

### rpcDisconnectTimeout
Timeout in minutes until the "Intiface not connected" rich presence disappears.

## User control stuff

### Allow direct user control
Allows users that are on the whitelist to directly control your toy using the commands at the end of this page! (you dont have to put yourself in the whitelist to use commands)

### UserIDs to grant command access to
Whitelist of people you want to have access to the commands. WARNING: DO NOT GIVE COMMAND ACCESS TO ANYONE YOU DO NOT TRUST

### Command prefix
The prefix for the command to be used. Example: ">."

## Command list
```
[command prefix] vibrate <amount> - Vibrate all devices
                    Example: [command prefix] vibrate 20
                    [command prefix] vibrate <deviceId> <amount> - Vibrate a specific device
                    Example: [command prefix] vibrate 1 20
                    [command prefix] vibrate <amount> <timeInMilliseconds> - Vibrate all devices for a specific duration
                    Example: [command prefix] vibrate 20 2000
                    [command prefix] vibrate <deviceId> <amount> <timeInMilliseconds> - Vibrate a specific device for a specific duration
                    Example: [command prefix] vibrate 1 20 2000

[command prefix] devices - List all connected devices
                    Example: [command prefix] devices

[command prefix] pattern <vibrationStrength> <intervalDuration> - Set a vibration pattern
                    Example: [command prefix] pattern 20 1000

[command prefix] stop - Stop all vibrations
```